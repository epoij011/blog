import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})

export class PostListItemComponent implements OnInit {

  @Input() postTitle: string;
  @Input() postContent: string;
  @Input() postLoveIts: number;
  @Input() postCreatedAt: Date;

  incLove() {
    this.postLoveIts++;
    console.log(this.postLoveIts);
  }

  decLove() {
    this.postLoveIts--;
  }

  constructor() {
  }

  ngOnInit() {
  }
}
