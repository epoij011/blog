import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  posts = [
    {
      title: "Mon premier post",
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mi leo, placerat at urna varius, pulvinar ullamcorper lectus.",
      loveIts: 5,
      createdAt: new Date()
    },
    {
      title: "Mon deuxième post",
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mi leo, placerat at urna varius, pulvinar ullamcorper lectus.",
      loveIts: -5,
      createdAt: new Date()
    },    
    {
      title: "Encore un post",
      content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi mi leo, placerat at urna varius, pulvinar ullamcorper lectus.",
      loveIts: 0,
      createdAt: new Date()
    }
  ]
}
